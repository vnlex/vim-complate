#!/bin/bash  

source $public_path'/config.sh'
date_time=`bash $public_path'/datetime.sh'`  
year=`date +%Y`  

head_note=`cat << EOF 
/*  
 * Copyright (c) $YEAR $complany. All Rights Resverd.
 * File         ：   $file
 * Author       ：   $author<$email>
 * Date         ：   $date_time  
 * Description  ：  
 * History      ：  
 *     
 */
EOF
`

