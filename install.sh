#!/bin/bash

vim_dir=$HOME/.vim
vim_config=$HOME/.vimrc


#删除原有文件
`test -d $vim_dir/templates | rm -rf $vim_dir/templates >/dev/null`
`rm -f $vim_dir/plugin/template.vim >/dev/null`
sed -i '/g:enable_template/d' $vim_config
sed -i '/g:template_dir/d' $vim_config
sed -i '/g:C_SourceCodeExtensions/d' $vim_config

if [ -n $1 ]&&[ "uninstall" = "$1" ];then
	echo "Uninstall complate!"
else
	
	`cp -rf ./templates $vim_dir/templates`
	`cp ./template.vim $vim_dir/plugin/`
	
	grep "g:enable_template" $vim_config >/dev/null
	if [ $? -eq 0 ]; then
	echo "Skip add config item"
	else
		`echo "let g:enable_template = 1
let g:template_dir = \"~/.vim/templates\"
let g:C_SourceCodeExtensions  = '' \"取消c.vim插件的文件头功能，将由 template.vim插件代替" >> $vim_config`
		
	fi
	echo "Install sucessed!"
fi


